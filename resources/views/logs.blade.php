<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Log Viewer</title>
</head>

<style>
    
    .form-group {
        display: inline-block;

    }

    .form-group input {
        width: 100px;
    }

    span {
        padding-left: 53px;
    }

</style>

<body>

    <h3 class="text-center mb-5">Log Viewer</h3>
    {{-- @dd($statistics) --}}
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">


                    
                    <span>Info</span>
                    <span>Warning</span>
                    <span>Error</span>
                    <span>Debug</span>
                    <span>Critical</span>
                    <span>Alert</span>
                    <span>Emergency</span>
                    <span>Notice</span>
                    <span>Update</span>

                    @foreach ($statistics as $key => $stat)
                        <form action="{{ route('static.update', $stat['id']) }}" method="post">
                            @csrf
                            @method('patch')
                            <div class="form-group">

                            Id: {{ $key }}
                            </div>
                            <div class="form-group">
                                <input type="number" name="info" value="{{ $stat['level']['info'] }}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="warning" value="{{ $stat['level']['warning'] }}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="error" value="{{ $stat['level']['error'] }}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="debug" value="{{ $stat['level']['debug'] }}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="critical" value="{{ $stat['level']['critical'] }}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="alert" value="{{ $stat['level']['alert'] }}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="emergency" value="{{ $stat['level']['emergency'] }}">
                            </div>
                            <div class="form-group">
                                <input type="number" name="notice" value="{{ $stat['level']['notice'] }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>

                    @endforeach

                    </tbody>

                </div>
            </div>
        </div>
    </div>


</body>

</html>
