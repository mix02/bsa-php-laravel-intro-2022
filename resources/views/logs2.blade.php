<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Log Viewer</title>
</head>

<style>
    input {
        width: 100px;
    }

    table td {
        vertical-align: middle !important;
    }
</style>
<body>

    <h3 class="text-center mb-5">Log Viewer</h3>
    {{-- @dd($statistics) --}}
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('static.update', 1) }}">
                        <table class="table table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>Info</th>
                                    <th>Warning</th>
                                    <th>Error</th>
                                    <th>Debug</th>
                                    <th>Critical</th>
                                    <th>Alert</th>
                                    <th>Emergency</th>
                                    <th>Notice</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($statistics as $key => $stat)
                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>
                                            <input type="number" name="info"
                                                value="{{ $stat['info'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="warning"
                                                value="{{ $stat['warning'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="error"
                                                value="{{ $stat['error'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="debug"
                                                value="{{ $stat['debug'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="critical"
                                                value="{{ $stat['critical'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="alert"
                                                value="{{ $stat['alert'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="emergency"
                                                value="{{ $stat['emergency'] }}">
                                        </td>
                                        <td>
                                            <input type="number" name="notice"
                                                value="{{ $stat['notice'] }}">
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>


</body>

</html>
