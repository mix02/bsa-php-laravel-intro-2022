<?php

namespace App\Http\Requests\Api\Log;

use Illuminate\Foundation\Http\FormRequest;

class GetLogsStatisticHttpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'info' => 'numeric',
            'warning' => 'numeric',
            'error' => 'numeric',
            'debug' => 'numeric',
            'critical' => 'numeric',
            'alert' => 'numeric',
            'emergency' => 'numeric',
            'notice' => 'numeric'

        ];
    }
}
