<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Models\Log;
use Illuminate\Database\Eloquent\Collection;

final class LogArrayPresenter
{
    public function present(Log $log): array
    {
        return [
            'level' => $log->getLevel(),
            'driver' => $log->getDriver(),
            'message' => $log->getMessage(),
            'trace' => $log->getTrace(),
            'channel' => $log->getChannel()
        ];
    }
    
    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Log $log) {
                    return $this->present($log);
                }
            )
            ->all();
    }

    public function presentLevelCollection(Collection $colletion): array
    {
        return $colletion
            ->map(
                function(Log $log) {
                    return ['id' => $log->id, 'level' => $log->getLevel()];
                }
            )
            ->all();
    }

}
