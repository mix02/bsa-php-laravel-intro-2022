<?php

namespace App\Http\Controllers;

use App\Action\Log\GetAllLogsAction;
use App\Action\Log\GetLogsByLevelAction;
use App\Action\Log\GetLogsByLevelRequest;
use App\Action\Log\GetLogsStatisticAction;
use App\Action\Log\GetLogsStatisticRequest;
use App\Action\Log\GetLogsStatisticUpdateAction;
use App\Http\Presenter\LogArrayPresenter;
use App\Http\Requests\Api\Log\GetLogsStatisticHttpRequest;
use App\Http\Response\ApiResponse;
use App\Repository\LogRepositoryInterface;

class LogController extends Controller
{

    public function getLogs(
        GetAllLogsAction $action,
        LogArrayPresenter $presenter
    ): ApiResponse
    {
        $logs = $action->execute()->getLogs();
        return ApiResponse::success($presenter->presentCollection($logs));
    }

    public function getLogsLevel(
        GetLogsByLevelAction $action,
        string $levelId
    ): ApiResponse
    {
        $logLevel = $action->execute(new GetLogsByLevelRequest((int) $levelId));
        return ApiResponse::success($logLevel->getLogs());
    }

    public function getStatistic(
        GetLogsStatisticAction $action,
        LogArrayPresenter $presenter
    )
    {

        $statistics = $action->execute()->getLogs();
        $statistics = $presenter->presentLevelCollection($statistics);
        return view('logs', [
            'statistics' => $statistics
        ]);
    }

    public function updateStatic(
        GetLogsStatisticHttpRequest $request,
        GetLogsStatisticUpdateAction $action,
        LogArrayPresenter $presenter,
        string $id
    ): ApiResponse
    {
    
        $response = $action->execute(new GetLogsStatisticRequest((int) $id, $request->validated()));


        return ApiResponse::success($presenter->present($response->getLog()));
       
    }



}
