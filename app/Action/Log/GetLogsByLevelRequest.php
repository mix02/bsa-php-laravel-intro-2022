<?php

declare(strict_types=1);

namespace App\Action\Log;

final class GetLogsByLevelRequest
{
    public function __construct(private int $levelId)
    {
    }

    public function getLevelId(): int
    {
        return $this->levelId;
    }
}
