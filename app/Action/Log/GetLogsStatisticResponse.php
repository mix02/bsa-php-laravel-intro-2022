<?php

namespace App\Action\Log;

use Illuminate\Database\Eloquent\Collection;

class GetLogsStatisticResponse
{

    public function __construct(public Collection $statistics)
    {
        
    }

    public function getLogs(): Collection
    {
        return $this->statistics;
    }
}
