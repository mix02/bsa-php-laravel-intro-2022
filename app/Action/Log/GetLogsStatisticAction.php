<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetLogsStatisticAction
{
    public function __construct()
    {
        
    }
    public function execute(): GetLogsStatisticResponse
    {
        
        $logRepository = app()->make(LogRepositoryInterface::class);
        $levels = $logRepository->findAll();
        return new GetLogsStatisticResponse($levels);

    }
}
