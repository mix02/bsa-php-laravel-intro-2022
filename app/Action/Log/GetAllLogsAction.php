<?php

namespace App\Action\Log;
use App\Repository\LogRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class GetAllLogsAction
{
    
    public function __construct()
    {
        
    }

    public function execute(): GetAllLogsResponse
    {
        $logRepository = app()->make(LogRepositoryInterface::class);
        
        return new GetAllLogsResponse(
            $logRepository->findAll()
        );

    }
}
