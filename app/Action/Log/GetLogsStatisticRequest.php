<?php

namespace App\Action\Log;

class GetLogsStatisticRequest
{
    public function __construct(public int $id, private array $level)
    {
        
    }


    public function getLevel(): array
    {
        return $this->level;
    }
}