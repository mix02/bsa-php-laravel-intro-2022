<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetLogsByLevelAction
{


    public function execute(GetLogsByLevelRequest $request): GetLogsByLevelResponse
    {
        
        $logRepository = app()->make(LogRepositoryInterface::class);


        return new GetLogsByLevelResponse($logRepository->findLogsLevel($request->getLevelId()));

    }
}
