<?php

namespace App\Action\Log;

class GetLogsByLevelResponse
{

    public function __construct(private array $level)
    {
        
    }
    public function getLogs(): array
    {
        return $this->level;
    }
}
