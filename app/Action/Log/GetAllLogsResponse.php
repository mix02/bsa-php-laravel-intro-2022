<?php

namespace App\Action\Log;

use Illuminate\Database\Eloquent\Collection;

class GetAllLogsResponse
{

    public function __construct(public Collection $logCollection)
    {  
    }
    public function getLogs(): Collection
    {
        return $this->logCollection;
    }

}
