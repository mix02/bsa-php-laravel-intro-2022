<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetLogsStatisticUpdateAction
{
    public function __construct(public LogRepositoryInterface $logRepositoryInterface)
    {
        
    }
    public function execute(GetLogsStatisticRequest $request)
    {

        $log = $this->logRepositoryInterface->findById($request->id);

        $log->level = $request->getLevel() ?: $log->level;

        $log = $this->logRepositoryInterface->save($log);


        return new GetLogsResponse($log);
    }
}
