<?php

namespace App\Action\Log;

use App\Models\Log;

class GetLogsResponse
{

    public function __construct(public Log $log)
    {  
        
    }
    public function getLog(): Log
    {
        return $this->log;
    }

}
