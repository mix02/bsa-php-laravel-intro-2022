<?php

namespace App\Repository;

use App\Models\Log;
use Illuminate\Database\Eloquent\Collection;

class LogRepository implements LogRepositoryInterface
{
    public function findAll(): Collection
    {
        return Log::all();
    }

    public function findById(int $id): Log
    {
        return Log::findOrFail($id);
    }

    public function findLogsLevel(int $levelId): array 
    {
        return Log::findOrFail($levelId, ['level'])->getLevel();
    }

    public function save(Log $log): Log
    {
        $log->save();

        return $log;
    }

}
