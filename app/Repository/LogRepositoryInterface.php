<?php

namespace App\Repository;

use App\Models\Log;
use Illuminate\Database\Eloquent\Collection;
use PhpParser\ErrorHandler\Collecting;

interface LogRepositoryInterface
{
    public function findAll(): Collection;

    public function findLogsLevel(int $levelId): array;

    public function findById(int $id): Log;

    public function save(Log $log): Log;
}
