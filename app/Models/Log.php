<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    use HasFactory;
    protected $table = 'logs';

    protected $casts = [
        'level' => 'array'
    ];

    protected $fillable = [
        'level',
        'driver',
        'message',
        'trace',
        'channel'
    ];


    public function getLevel(): array
    {
        return $this->level;
    }

    public function getDriver(): string
    {
        return $this->driver;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getTrace(): string
    {
        return $this->trace;
    }

    public function getChannel(): ?string
    {
        return $this->channel;
    }
    
}
