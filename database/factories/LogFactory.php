<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LogFactory extends Factory
{
    public function definition()
    {
        return [
            'level' => [
                'info' => $this->faker->numberBetween(1, 10),
                'warning' => $this->faker->numberBetween(1, 10),
                'error' => $this->faker->numberBetween(1, 10),
                'debug' => $this->faker->numberBetween(1, 10),
                'critical' => $this->faker->numberBetween(1, 10),
                'alert' => $this->faker->numberBetween(1, 10),
                'emergency' => $this->faker->numberBetween(1, 10),
                'notice' => $this->faker->numberBetween(1, 10),
            ],
            'driver' => 'database',
            'message' => $this->faker->name,
            'trace' => $this->faker->text,
            'channel' => 'default'
        ];
    }
}
